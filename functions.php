<?php //Start building your awesome child theme functions

add_action( 'wp_enqueue_scripts', 'shopkeeper_enqueue_styles', 99 );
function shopkeeper_enqueue_styles() {

    // enqueue parent styles
    wp_enqueue_style( 'shopkeeper-icon-font', get_template_directory_uri() . '/inc/fonts/shopkeeper-icon-font/style.css' );
	wp_enqueue_style( 'shopkeeper-styles', get_template_directory_uri() .'/css/styles.css' );
    wp_enqueue_style( 'shopkeeper-default-style', get_template_directory_uri() .'/style.css' );

    // enqueue child styles
    wp_enqueue_style( 'shopkeeper-child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( 'shopkeeper-default-style' ),
        wp_get_theme()->get('Version')
    );

	// enqueue RTL styles
    if( is_rtl() ) {
    	wp_enqueue_style( 'shopkeeper-child-rtl-styles',  get_template_directory_uri() . '/rtl.css', array( 'shopkeeper-styles' ), wp_get_theme()->get('Version') );
    }
}

add_filter( 'woocommerce_available_payment_gateways', 'bbloomer_gateway_disable_shipping_326' );
function bbloomer_gateway_disable_shipping_326( $available_gateways ) {
   if ( ! is_admin() ) {
      $chosen_methods = WC()->session->get( 'chosen_shipping_methods' );
      $chosen_shipping = $chosen_methods[0];
      if ( isset( $available_gateways['bacs'] ) && 0 === strpos( $chosen_shipping, 'flexible_shipping_10_7' ) ) {
         unset( $available_gateways['bacs'] );
      }
   }
   return $available_gateways;
}

add_filter( 'body_class', 'custom_body_class_filter' );
function custom_body_class_filter( $classes ) {
    if( is_page_template('page-templates/home.php') )
        $classes[] = 'archive woocommerce';
    return $classes;
}

add_action('wp_head', 'wp_custom_head');
function wp_custom_head(){ ?>
    <link rel="preload" as="font" href="https://grebnesor.sk/wp-content/themes/shopkeeper/inc/fonts/shopkeeper-icon-font/fonts/Shopkeeper-Icon-Font.woff2" type="font/woff2" crossorigin importance="auto">
<?php
};