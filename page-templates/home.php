<?php
/**
 * Template Name: Home
 */

defined( 'ABSPATH' ) || exit;

remove_filter( 'woocommerce_product_loop_start', 'woocommerce_maybe_show_product_subcategories' );

remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
remove_action( 'woocommerce_no_products_found', 'wc_no_products_found', 10 );

get_header();

if(!function_exists('wc_get_products')) {
  return;
}

$paged                   = (get_query_var('page')) ? absint(get_query_var('page')) : 1;
$ordering                = WC()->query->get_catalog_ordering_args();
$ordering['orderby']     = array_shift(explode(' ', $ordering['orderby']));
$ordering['orderby']     = stristr($ordering['orderby'], 'price') ? 'meta_value_num' : $ordering['orderby'];
$products_per_page       = apply_filters('loop_shop_per_page', wc_get_default_products_per_row() * wc_get_default_product_rows_per_page());

$featured_products       = wc_get_products(array(
  'meta_key'             => '_price',
  'visibility'           => 'visible',
  'status'               => user_can(get_current_user_id(), 'read_private_posts') ? ['publish', 'private'] : 'publish',
  'limit'                => $products_per_page,
  'page'                 => $paged,
  'paginate'             => true,
  'return'               => 'ids',
  'orderby'              => $ordering['orderby'],
  'order'                => $ordering['order'],
));

wc_set_loop_prop('current_page', $paged);
wc_set_loop_prop('is_paginated', wc_string_to_bool(true));
wc_set_loop_prop('page_template', get_page_template_slug());
wc_set_loop_prop('per_page', $products_per_page);
wc_set_loop_prop('total', $featured_products->total);
wc_set_loop_prop('total_pages', $featured_products->max_num_pages);
?>

<div id="primary" class="content-area shop-page p-0">
  <div class="row">
		<div class="large-12 columns">
      <div id="content" class="site-content" role="main">
				<div class="row">
					<div class="large-12 columns">
            <div class="large-12 mobile-columns-2 ">
              <div class="vc_row wpb_row vc_row-fluid wpb_animate_when_almost_visible wpb_fadeIn fadeIn vc_custom_1582027392969 wpb_start_animation animated">
                <div class="wpb_column vc_column_container vc_col-sm-12">
                  <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                      <div class="row">
                        <div class="categories_grid">
                          <div class="category_four_cat_1">
                            <div class="category_grid_box">
                              <span class="category_item_bkg" style="background-image:url(https://grebnesor.sk/wp-content/uploads/2017/02/portfolio-gcollection-07.jpg)"></span>
                              <a href="https://grebnesor.sk/sk_sk/kategoria-produktu/damske-oblecenie/" class="category_item">
                                <span class="category_name">Dámske oblečenie</span>
                              </a>
                            </div>
                          </div>                          
                          <div class="category_four_cat_2">
                            <div class="category_grid_box">
                            <span class="category_item_bkg woman-shoes" style="background-image:url(https://grebnesor.sk/wp-content/uploads/2020/02/comfortable-women-shoes.jpg)"></span>
                              <a href="https://grebnesor.sk/sk_sk/kategoria-produktu/damska-obuv/" class="category_item">
                                <span class="category_name">Dámska obuv</span>
                              </a>
                            </div>
                          </div>                          
                          <div class="category_four_cat_3">
                            <div class="category_grid_box">                             
                              <span class="category_item_bkg man-clothes" style="background-image:url(https://grebnesor.sk/wp-content/uploads/2020/02/mens_banner.jpg)"></span>
                              <a href="https://grebnesor.sk/sk_sk/kategoria-produktu/panske-oblecenie/" class="category_item">
                                <span class="category_name">Pánske oblečenie</span>
                              </a>
                            </div>
                          </div>                          
                          <div class="category_four_cat_4">
                            <div class="category_grid_box">
                              <span class="category_item_bkg" style="background-image:url(https://grebnesor.sk/wp-content/uploads/2020/03/xCorax-Sneakers-Spring-Summer-2019.jpg.pagespeed.ic_.g4RRN3Pg54.jpg)"></span>
                              <a href="https://grebnesor.sk/sk_sk/kategoria-produktu/panska-obuv/" class="category_item">
                                <span class="category_name">Pánska obuv</span>
                              </a>
                            </div>
                          </div>                          
                          <div class="clearfix"></div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="wpb_text_column wpb_content_element  vc_custom_1592222714313">
                  <div class="wpb_wrapper">
                    <h3 style="text-align: center;">Novinky</h3>
                  </div>
                </div>
              <?php
              if($featured_products) {
                do_action('woocommerce_before_shop_loop');
                woocommerce_product_loop_start();
                  foreach($featured_products->products as $featured_product) {
                    $post_object = get_post($featured_product);
                    setup_postdata($GLOBALS['post'] = $post_object);
                    wc_get_template_part('content', 'product');
                  }
                  wp_reset_postdata();
                woocommerce_product_loop_end();
              ?>
            </div> 
            <div class="woocommerce-after-shop-loop-wrapper">
              <?php
                do_action( 'woocommerce_after_shop_loop' );
              ?>
            </div>
            <?php
            } else {
              do_action('woocommerce_no_products_found');
            }
            ?> 
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php
get_footer(); ?>
